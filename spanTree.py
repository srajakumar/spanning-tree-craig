# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 13:33:01 2018

@author: rajaks
"""

#!/usr/bin/python
from rancidcmd import RancidCmd
from openpyxl import load_workbook,Workbook
import argparse

import re

# Get the Switch list from the given excel sheet
def getSwitchNames():
    
    for sht in wb.sheetnames:
        g['{0}lst'.format(sht)]=[]
        ws = wb[sht]
        col = ws['A']
        for val in range(1,len(col)+1):
            g['{0}lst'.format(sht)].append(ws['A'+str(val)].value )

#Parse the show commands for the list of switches.
def parseShowCmds():
    cisco_cmd = ['show spanning-tree active' , 'show spanning-tree summary']
    xtreme_cmd = ['show spantree mstcfgid' , 'show spantree spanguard', 'show spantree stats active']
    arista_cmd = ["show run | s bpdu" , "show spanning-tree mst configuration" ,"show spanning-tree mst"]
    hpe_cmd = ['show spanning-tree']
    configwb = Workbook() 
    for sht in wb.sheetnames:
        g['{0}sht'.format(sht)]=configwb.create_sheet(sht)        
        #CISCO Module       
        if sht.strip() == "Cisco":
            #i=1
            citer=1
            print "Creating CISCO sheet and formatting the output" 
            for item in g['{0}lst'.format(sht)]:
		i=1
                #rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                for cmd in cisco_cmd:
                    rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                    spanout= rancid.execute(cmd)
                    for line in spanout['std_out'].splitlines():
                        if 'Time' in line or re.search(r'VLAN+[0-9]{1,4} ',line):
                            continue
                        else:
                            g['{0}sht'.format(sht)].cell(row=i,column=citer).value=line
                            i=i+1
                citer=citer+1
        #ARISTA Module
        elif sht == "Arista":
            #i=1
            citer=1
	    print "Creating ARISTA sheet and formatting the output"
            #csht = configwb.create_sheet(sht)
            for item in g['{0}lst'.format(sht)]:  
		i=1
                #rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                for cmd in arista_cmd:
                    rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                    spanout= rancid.execute(cmd)
                    for line in spanout['std_out'].splitlines():
                        if re.search(r"(Po+[0-9]{1,2}|Et+[0-9]{1,2})",line):
                            continue
                        else:
                            g['{0}sht'.format(sht)].cell(row=i,column=citer).value=line
                            i=i+1
                citer=citer+1
        #Enterasys Module
        elif sht == "Enterasys":
            #i=1
            citer=1
	    print "Creating ENTERASYS sheet and formatting the output"
            #csht = configwb.create_sheet(sht)
            for item in g['{0}lst'.format(sht)]:    
		i=1
                #rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                for cmd in xtreme_cmd:
                    rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                    spanout= rancid.execute(cmd)
                    for line in spanout['std_out'].splitlines():
                        if re.search(r'(tg.)',line) or re.search(r"(Age|Time|Delay)",line):
                            continue
                        else:
                            g['{0}sht'.format(sht)].cell(row=i,column=citer).value=line
                            i=i+1
                citer=citer+1
        #HP Module
        elif sht == "HP":
            #i=1
            citer=1
	    print "Creating HP sheet and formatting the output"
            #csht = configwb.create_sheet(sht)
            for item in g['{0}lst'.format(sht)]:
		i=1
                #rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                for cmd in hpe_cmd:
                    rancid = RancidCmd(login='/usr/libexec/rancid/hlogin',address=item)
                    spanout= rancid.execute(cmd)
                    for line in spanout['std_out'].splitlines():
			if re.search(r"[;189R]",line):
			    re.sub(r'[;189R]',' ',line)

                        elif re.search(r"(Age|Hope|Delay|IST|Force|TCN|PVST)",line) or re.search(r'(A+[1-9]{1,3})',line) or line in ['\n', '\r\n']:
                            continue
                        else:
                            g['{0}sht'.format(sht)].cell(row=i,column=citer).value=line
                            i=i+1
                citer=citer+1
    



        configwb.save(args.OutFile)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('OutFile')
    parser.add_argument('InFile')
    try:
	    args = parser.parse_args()
    except IOError, msg:
	    parser.error(str(msg))
    g=globals()
    wb=load_workbook(args.InFile)
    switchDet = getSwitchNames()
    parseShowCmds()
