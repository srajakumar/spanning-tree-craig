# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 12:10:35 2018

@author: rajaks
"""

import pandas as pd
from rancidcmd import RancidCmd
import re
import sys

#Switch tab values initiated as list. Do remove accordingly in case if any of the switch is not present 
#in the source excel
lst=["Arista","Cisco","HPE","Extreme"]

dvclst=[]    
vlanlst=[]
modlst=[]
prilst=[]
iplst=[]

#Create initial panda data frame with empty list
dfw = pd.DataFrame.from_dict({'Device':dvclst,'VLAN':vlanlst,'STP Mode':modlst,'STP Priority':prilst,'STP Root':iplst})

ipfile = sys.argv[1]
opfile = ipfile + "_output"

for switch in lst:
    df = switch+"_df"
    #Source Excel file. Change in case need to run with another source file.
    df = pd.read_excel(ipfile,sheet_name=switch,header=None)
    slst=df.to_string(index=False)
    for val in slst.split("\n"):
        if  val.strip() != "0" and val != "" and switch in ("Arista","Cisco"):
            #Module which Extracts values for device under Arista and Cisco switch
            print "Extracting for device " +val.strip() +" Switch " + switch
            # The list should be made null for every device in each switch
            dvclst=[]
            vlanlst=[]
            modlst=[] 
            prilst=[]
            iplst=[]
            rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=val.strip())
            spanout= rancid.execute("show spanning-tree")
            lineit = spanout['std_out'].splitlines()
            dvclst.append(val.strip())
            for ind,line in enumerate(lineit):
                if re.search(r"stp",line):
                    modlst.append(line.split(" tree enabled protocol ")[1])
                if re.search(r"VLAN[0-9]{0,4}",line):
                    vlanlst.append(line)
                if re.search(r'(?<=Priority)\s+\d+',line) and re.search(r'^\s+Bridge',line): 
                    prilst.append(re.search(r'(?<=Priority)\s+\d+',line).group(0).strip())
                if re.search(r'^\s+Root ID',line):
                    iplst.append(lineit[ind+1].split("Address")[1].strip())
            #Below code snippet is present to add null values to excel row in case if valid entries are not
            #present it appends blank value.
            while len(dvclst)<len(vlanlst):
                dvclst.append("")
            while len(vlanlst)<len(dvclst):
                vlanlst.append("")
            while len(modlst)<len(dvclst):
                modlst.append("")
            while len(prilst)<len(dvclst):
                prilst.append("")
            while len(iplst)<len(dvclst):
                iplst.append("")
            #Add value and append the data frame to the initial empty list
            dfwa = pd.DataFrame({'Device':dvclst,
                                 'VLAN':vlanlst,
                                 'STP Mode':modlst,
                                 'STP Priority':prilst,
                                 'STP Root':iplst})
            dfw=dfw.append(dfwa,ignore_index=True)
            #Write to excel the dataframe and save in the output file "opfile"
            writer=pd.ExcelWriter(opfile,engine='xlsxwriter')
            dfw.to_excel(writer,'Sheet1',index=False)
            writer.save
        elif  val.strip() != "0" and val != "" and switch in ("Aruba","HPE"):
            #Module which Extracts values for device under Aruba and HPE switch
            print "Extracting for device " +val.strip() +" Switch " + switch
            dvclst=[]
            vlanlst=[]
            modlst=[]
            prilst=[]
            iplst=[]
            rancid = RancidCmd(login='/usr/libexec/rancid/hlogin',address=val.strip())
            spanout= rancid.execute("show spanning-tree")
            lineit = spanout['std_out'].splitlines()
            dvclst.append(val.strip())
            for ind,line in enumerate(lineit):
                if re.search(r"Force Version",line):
                    mode=line.split(":")[1].strip()
                    modlst.append(mode[:5])
                if re.search(r'Switch Priority',line) : 
                    prilst.append(line.split(":")[1].strip())
                if re.search(r'CST :Root MAC Address',line):
                    iplst.append(line.split(":")[1].strip())
            while len(dvclst)<len(vlanlst):
                dvclst.append("")
            while len(vlanlst)<len(dvclst):
                vlanlst.append("")
            while len(modlst)<len(dvclst):
                modlst.append("")
            while len(prilst)<len(dvclst):
                prilst.append("")
            while len(iplst)<len(dvclst):
                iplst.append("")
            dfwa = pd.DataFrame({'Device':dvclst,
                                 'VLAN':vlanlst,
                                 'STP Mode':modlst,
                                 'STP Priority':prilst,
                                 'STP Root':iplst})
            dfw=dfw.append(dfwa,ignore_index=True)
            writer=pd.ExcelWriter(opfile,engine='xlsxwriter')
            dfw.to_excel(writer,'Sheet1',index=False)
            writer.save
        elif  val.strip() != "0" and val != "" and switch in ("Enterasys","Extreme"):
            #Module which Extracts values for device under Aruba and HPE switch
            print "Extracting for device " +val.strip() +" Switch " + switch
            dvclst=[]
            vlanlst=[]
            modlst=[]
            prilst=[]
            iplst=[]
            rancid = RancidCmd(login='/usr/libexec/rancid/hlogin',address=val.strip())
            spanout= rancid.execute("show spantree version")
            lineit = spanout['std_out'].splitlines()
            if re.search(r"Force Version",line):
                modlst.append(line.split("is")[1].strip())
            rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=val.strip())
            spanout= rancid.execute("show spantree stats active")
            lineit = spanout['std_out'].splitlines()
            dvclst.append(val.strip())
            for ind,line in enumerate(lineit):
                if re.search(r"Bridge ID Priority",line):
                    prilst.append(line.split("-")[1].strip())
                if re.search(r'Designated Root MacAddr',line) : 
                    iplst.append(line.split("- ")[1].strip())
            while len(dvclst)<len(vlanlst):
                dvclst.append("")
            while len(vlanlst)<len(dvclst):
                vlanlst.append("")
            while len(modlst)<len(dvclst):
                modlst.append("")
            while len(prilst)<len(dvclst):
                prilst.append("")
            while len(iplst)<len(dvclst):
                iplst.append("")
            dfwa = pd.DataFrame({'Device':dvclst,
                                 'VLAN':vlanlst,
                                 'STP Mode':modlst,
                                 'STP Priority':prilst,
                                 'STP Root':iplst})
            dfw=dfw.append(dfwa,ignore_index=True)
            writer=pd.ExcelWriter(opfile,engine='xlsxwriter')
            dfw.to_excel(writer,'Sheet1',index=False)
            writer.save
