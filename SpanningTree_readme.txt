# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 01:59:37 2018

@author: rajaks
"""

Pre-requisites:
==============

1. rancidcmd

Please follow the link for installation related issues,
https://pypi.org/project/rancidcmd/

Please refer the documentation for more info on rancidcmd,
https://media.readthedocs.org/pdf/rancidcmd/latest/rancidcmd.pdf

2. openpyxl
   == pip install openpyxl
3. argparse

Files required:
==============

1. spanTree.py = Main python script which executes the code.
2. STP_Switch.xlsx = Excel file which contains the list of switches.

How to run:
==========

Command to execute the script,

python spanTree.py <argument1> <argument2>

<argument1> - Output excel file which has to be generated
<argument2> - Input excel file which contains the list of Switch entries. Eg:STP_Switch.xlsx