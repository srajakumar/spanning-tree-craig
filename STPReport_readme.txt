# -*- coding: utf-8 -*-
"""
Created on Sat Jul 28 01:45:35 2018

@author: rajaks
"""

Pre-requisites:
==============

1. rancidcmd

Please follow the link for installation related issues,
https://pypi.org/project/rancidcmd/

Please refer the documentation for more info on rancidcmd,
https://media.readthedocs.org/pdf/rancidcmd/latest/rancidcmd.pdf

2. pandas
   == pip install pandas
3. xlrdwriter (pandas use this writer)
   == pip install xlrdwriter
4. re
5. sys

Files required:
==============

1. STPReport.py = Main python script which executes the code.
2. stp-report-template.xlsx = Excel file which contains the list of switches.

How to run:
==========

Command to execute the script,

python STPReport.py <argument1> 

<argument1> - Input excel file which contains the list of Switch entries. Eg:stp-report-template.xlsx

Note:
====

Please do note if there are no seperate sheet for ["Arista","Cisco","HPE","Extreme"] in the input file,
delete the entry from the below given line in "STPReport.py" line 15,
        lst=["Arista","Cisco","HPE","Extreme"]
So that the entry should be removed if there is no entry in the excel file.

Improvement to script:
=====================
1. Planning to take this list from the excel sheet values.
2. As it consumes time planning to make it parallelized.